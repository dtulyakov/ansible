Ansible

## Install [ansible ubuntu](http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-ubuntu)

```bash

sudo apt-get update -q \
  && sudo apt-get install software-properties-common -y \
  && sudo apt-add-repository ppa:ansible/ansible -y \
  && sudo apt-get update -q \
  && sudo apt-get install ansible -y
```

Или так

```bas
sudo pip3 install ansible
```

## Использование

```bash

ansible-playbook playbooks/zabbix.yml -l serverprod01
```

```bash

ansible-playbook playbooks/adduser_ansible.yml -l serverprod01 -u username --ask-sudo-pass
```
Что бы потестировальл на локалхосте плейбук grub с пользователем denis и запросом пароля в режиме чек и выводом изменений

```sh
ansible-playbook playbooks/grub.yml -i "localhost," -c local -u denis --ask-become-pass --skip-tags "notification" -DC
```

Для пропуска некоторых тасков надо указать `--skip-tags` при условии что они есть (можно указывать несколько через запятую)

```bash
ansible-playbook playbooks/tmux.yml -l superprodserver02 --skip-tags "notification"
```

Аналогично для проигрывания только тех тасков которые нужны или сгруппированы в теги запускать так

```bash
ansible-playbook playbooks/docker.yml -l devserver10 --tags "notification"
```

|ключ|параметр                                     |
|----|---------------------------------------------|
|-i  |выбрать другой inventory                     |
|-l  |указать группу (по дефолту прокатывается all)|
|-u  |ssh пользователь                             |
|-e  |extra vars надо передовать в JSON            |
